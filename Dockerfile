FROM debian:11-slim

RUN apt-get -y update && apt-get -y upgrade; \
apt-get install vim --without-recommends && rm -rf /var/lib/apt/lists/*

CMD ["/bin/bash"]
